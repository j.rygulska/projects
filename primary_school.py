class Person:
    def __init__(self, name, surname):
        self.name = name
        self.surname = surname

    def introduce(self):
        return f"I am {self.name} {self.surname}."

class Teacher(Person):
    def __init__(self, name, surname, subject):
        super().__init__(name, surname)
        self.subject = subject
        self.marks = {}

    def introduce(self):
        return f"{super().introduce()} I teach the subject: {self.subject}."

    def add_mark(self, student, mark):
        if student not in self.marks:
            self.marks[student] = []
        self.marks[student].append(mark)
        return f"Mark: {mark}, added for student: {student}."

    def average_mark(self, student):
        if student in self.marks and self.marks[student]:
            return f"The average mark is: {sum(self.marks[student]) / len(self.marks[student])}"
        else:
            return "No marks."

class Student(Person):
    def __init__(self, name, surname, class_name):
        super().__init__(name, surname)
        self.class_name = class_name

    def introduce(self):
        return f"{super().introduce()} I am student of class: {self.class_name}."

    def pass_exam(self, mark):
        if mark >= 3:
            return f"{self.name} {self.surname} passed the exam."
        else:
            return f"{self.name} {self.surname} didn't pass the exam."

class Headmaster(Person):
    def __init__(self, name, surname, school):
        super().__init__(name, surname)
        self.school = school

    def introduce(self):
        return f"{super().introduce()} I am a Headmaster of {self.school}."